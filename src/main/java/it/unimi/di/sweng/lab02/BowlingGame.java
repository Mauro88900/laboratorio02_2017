package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {
	
	private int[] rolls = new int[24];
	private int score = 0;
	private int currentRoll = 0;
	
	@Override
	public void roll(int pins) {
		if( (currentRoll%2==0)&&(pins==10) ){
			rolls[currentRoll++]=pins;
			rolls[currentRoll++]=0;
		}else
			rolls[currentRoll++]=pins;
	}

	@Override
	public int score() {
		for(int i=0;i<currentRoll;i++){
			if( isStrike(i) ) {
				score += 10 + rolls[i+2]+rolls[i+3];
				i++;
			} else if( isSpare(i) ) {
				this.score += 10 + rolls[i+2];
				i++;
			} else 
				this.score+=rolls[i];
		}
		if (isPerfectGame(currentRoll)) //Sistemare
			this.score=300;
		
		return this.score;
	}
	
	private boolean isSpare(int currentRoll) {
		return currentRoll%2==0 && currentRoll<18 && rolls[currentRoll]+rolls[currentRoll+1]==10;
	}

	public boolean isStrike(int currentRoll){
		return currentRoll<18 && currentRoll%2==0 && rolls[currentRoll]==10;
	}
	
	public boolean isPerfectGame(int currentRoll){
		int numeroStrike=0;
		for (int i=0;i<currentRoll;i+=2)
			if (rolls[i]==10)
				numeroStrike++;
		return numeroStrike==12;
	}

}

/*
	
*/
